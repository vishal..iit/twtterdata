import React, { Component } from 'react';
import { Navbar,NavbarBrand } from 'reactstrap';
import Search from './components/SearchComponent';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar dark color="primary">
        <div className="container">
          <NavbarBrand href="/">
              <b>Twitter</b>
          </NavbarBrand>
        </div>
        </Navbar>
        <Search /> 
      </div>
    );
  }
}

export default App;
