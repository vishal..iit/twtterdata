export const searchTwitterPending = ({ query }) => ({
	type: 'SEARCH_TWITTER_PENDING',
	query
})

export const searchTwitterSuccess = ({ tweets }) => ({
	type: 'SEARCH_TWITTER_SUCCESS',
	tweets
})

export const searchTwitterFailure = () => ({
	type: 'SEARCH_TWITTER_FAILURE'
})

export const alterKeyword = ({ query }) => ({
	type: 'ALTER_KEYWORD',
	query
})
