import React, { Component } from 'react';
import{ connect } from 'react-redux';
import * as actions from '../actions';

import Tweet from './Tweet';

class Search extends Component {
    constructor(props) {
        super(props);
        console.log('props', props)
        this.search = this.search.bind(this);
    }

    search({ target: { value }}) {
      console.log('searching', value);
      this.props.searchTweets({ query: value })
    }

    alterKeyword = ({ target: { value }}) => {
      console.log('altering', value);
      this.props.alterKeyword({ query: value })
    }

    render() {
        return (
          <div className="content">
            <div className="container">
            <section className="section" style={{ marginTop: `10px` }}>
                <form className="form" id="addItemForm">
                  <input
                    type="text"
                    className="input"
                    id="addInput"
                    placeholder="Enter keywords"
                    // onChange={this.search}
                    onChange={this.alterKeyword}
                  />
                </form>
              </section>
              <hr />
              <div>
                {this.props.tweets.map(tweet => <Tweet key={tweet.id} tweet={tweet} />)}
              </div>
            </div>
          </div>
        );
    }
}

const mapStateToProps = state => ({
  tweets: state.search.tweets,
  loading: state.search.loading,
  error: state.search.error
})

const mapDispatchToProps = {
  searchTweets: actions.searchTwitterPending,
  alterKeyword: actions.alterKeyword
}


export default connect(mapStateToProps, mapDispatchToProps)(Search);