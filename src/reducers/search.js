const INITIAL_STATE = {
	loading: false,
	error: false,
	tweets: []
}

const search = (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case 'SEARCH_TWITTER_PENDING':
			return {
				...state,
				loading: true,
				error: false
			}
		case 'SEARCH_TWITTER_SUCCESS':
			return {
				...state,
				loading: false,
				tweets: [ ...action.tweets, ...state.tweets]
			}
		case 'SEARCH_TWITTER_FAILURE':
			return {
				...state,
				loading: false,
				error: true
			}
		default:
			return state;
	}
}

export default search;