import { fork } from 'redux-saga/effects';
import search from './search';

function* rootSaga() {
	yield fork(search);
}

export default rootSaga;
