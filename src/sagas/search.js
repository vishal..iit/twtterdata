import { fork, call, takeLatest, take } from 'redux-saga/effects';
import { eventChannel, END } from 'redux-saga';
import Twit from 'twit';
import { twitterConfig } from '../config'

const T = new Twit(twitterConfig);

// function* searchWatcher() {
// 	yield takeLatest('SEARCH_TWITTER_PENDING', searchForTweets)
// }

function* alterKeyword({ query }) {
	const chan = yield call(twitterStream, query);
	try {
		while (true) {
			const tweet = yield take(chan);
			console.log('tweet received', tweet)
			// Add the tweet to state
		}
	} finally {
		console.log('closed');
	}
}

function* alterKeywordWatcher() {
	yield takeLatest('ALTER_KEYWORD', alterKeyword);
}

function twitterStream(query) {
	console.log('instream', query)
	return eventChannel(emitter => {
		const stream = T.stream('statuses/filter', { track: query })
		stream.on('tweet', function (tweet) {
			emitter({ tweet });
		})
		return stream.stop
	})
}

// export default searchWatcher;
export default alterKeywordWatcher;
